const get = require('../util/get');
const constructFieldTokens = require('../util/construct-field-tokens');
const getFields = require('../util/get-fields');
const getData = require('../util/get-data');
const { HTTP_CODES } = require('../constants');

const schema = {
    body: {
        type: ['object', 'null'],
        properties: {
            fields: { 
                description: 'a curly brace string formatted expression of comma separated fields of length N deep',
                type: 'string',
                pattern: '^([\\\n\\\t\\\s]*{)([A-z0-9_,{}\\\n\\\t\\\s]+)(}[\\\n\\\t\\\s]*)$'
            },
            filters: { 
                description: 'an array of object filters to find the document(s) to fetch',
                type: 'array' 
            },
        },
    }
}

module.exports = async (fastify) => {
    fastify.post(
        '/fetch/:label', { schema }, async (request, reply) => {
            const port = fastify.server.address().port;
            const { label } = request.params;
            let { fields, filters = [] } = get(request, 'body', {});

            const fieldTokens = constructFieldTokens(fields);
            fields = getFields(fieldTokens);

            reply.code(HTTP_CODES.OK).send( await getData(label, fields, filters, request.query, port) );
        }
    );

    fastify.get(
        '/fetch/:label', {}, async (request, reply) => {
            const port = fastify.server.address().port;
            const { label } = request.params;
            reply.code(HTTP_CODES.OK).send( await getData(label, [], [], request.query, port) );
        }
    );
}