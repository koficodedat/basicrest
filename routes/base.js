const getCollections = require('../util/get-collections');
const getCount = require('../util/get-count');
const mapSync = require('../util/sync-map');
const get = require('../util/get');
const getFileSize = require('../util/get-file-size');

const { LOCALIZATION } = require('../constants');
const { HTTP_CODES } = require('../constants');

module.exports = async (fastify) => {
  fastify.get(
    '/', async () => LOCALIZATION.EN.THANKS_FOR_USING_BASIC_REST
  );

  fastify.get(
    '/state', {}, async (request, reply) => {
        const port = fastify.server.address().port;

        const collections = await getCollections(port);
        
        reply
          .code(HTTP_CODES.OK)
          .send({
            status: LOCALIZATION.EN.SERVER_IS_OPTIMAL,
            dbStats: {
              collections: await mapSync(
                collections, async collection => ({
                  name: collection._$ref,
                  columns: get(collection, 'fields', '').split(',')
                    .reduce(
                      (columns, column) => {
                        !column.includes('.') && columns.push(column);
                        return columns;
                      },
                      []
                    ),
                  count: await getCount(collection._$ref, [], port),
                })
              ),
              dbSize: getFileSize(port),
            }
          });
    }
  );
}