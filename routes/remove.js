const removeData = require('../util/remove-data');
const { HTTP_CODES } = require('../constants');

const schema = {
    body: { 
      type: 'object',
      properties: {
        filters: { 
          description: 'an array of object filters to find the document(s) to remove',
          type: 'array' 
        },
      },
      required: ['filters']
    }
}

module.exports = async (fastify) => {
  fastify.delete(
    '/remove/:label', { schema }, async (request, reply) => {
      const port = fastify.server.address().port;
      const { label } = request.params;
      const { filters } = request.body;
      reply.code(HTTP_CODES.OK).send( await removeData(label, filters, port) );
    }
  );
}