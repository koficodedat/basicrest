const updateData = require('../util/update-data');
const { HTTP_CODES } = require('../constants');

const schema = {
    body: {
      type: 'object',
      properties: {
        filters: { 
          description: 'an array of object filters to find the document(s) to update',
          type: 'array' 
        },
        update: { 
          description: 'an object containing the key - value pair of entries to amend or add to the filtered document(s)',
          type: 'object' 
        },
      },
      required: ['filters', 'update']
    }
}

module.exports = async (fastify) => {
  fastify.put(
    '/update/:label', { schema }, async (request, reply) => {
      const port = fastify.server.address().port;
      const { label } = request.params;
      const { filters, update } = request.body;
      reply.code(HTTP_CODES.OK).send( await updateData(label, filters, update, request.query, port) );
    }
  );
}