const get = require('../util/get');
const getCount = require('../util/get-count');
const { HTTP_CODES } = require('../constants');

const schema = {
    body: {
        type: ['object', 'null'],
        properties: {
            filters: { type: 'array' },
        }
    }
}

module.exports = async (fastify) => {
    fastify.post(
        '/count/:label', { schema }, async (request, reply) => {
            const port = fastify.server.address().port;
            const { label } = request.params;
            let { filters = [] } = get(request, 'body', {});

            reply.code(HTTP_CODES.OK).send( await getCount(label, filters, port) );
        }
    );

    fastify.get(
        '/count/:label', {}, async (request, reply) => {
            const port = fastify.server.address().port;
            const { label } = request.params;
            reply.code(HTTP_CODES.OK).send( await getCount(label, [], port) );
        }
    );
}