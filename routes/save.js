const createData = require('../util/create-data');
const { HTTP_CODES } = require('../constants');

const schema = {
    body: { 
      description: 'an object or array of object documents for saving',
      type: ['object', 'array']
    }
}

module.exports = async (fastify) => {
  fastify.post(
    '/save/:label', { schema }, async (request, reply) => {
      const port = fastify.server.address().port;
      const { label } = request.params;
      reply.code(HTTP_CODES.OK).send( await createData(label, request.body, port) );
    }
  );
}