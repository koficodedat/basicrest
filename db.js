const Datastore = require('nedb-promises');

const { FILE_SYSTEM } = require('./constants');

module.exports = port => new Datastore({ filename: `${FILE_SYSTEM.DATA_DIR_PATH}/${port}` });