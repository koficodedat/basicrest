const CONSTANTS = {
    HTTP_CODES: {
        OK: 200,
        BAD_REQUEST: 400,
        INTERNAL_SERVER_ERROR: 500,
        NOT_IMPLEMENTED: 501,
    },
    LOCALIZATION: {
        EN: {
            // infos
            THANKS_FOR_USING_BASIC_REST: 'Thanks for using BasicREST',
            SERVER_IS_OPTIMAL: 'Server is optimal',

            // errors
            NEEDS_PORT_NUMBER_FOR_SERVER: 'Needs a port number to create a Server',
            PORT_MUST_BE_A_NUMBER: 'Port must be a number',
            OVERRIDE_MUST_BE_A_BOOLEAN: 'Override must be a boolean',
            CANNOT_SAVE_EMPTY_DATA: 'Cannot save empty data',
            ERROR_CREATING_DATA: 'Error creating data',
            ERROR_GETTING_COUNT: 'Error getting count',
            ERROR_GETTING_DATA: 'Error getting data',
            ERROR_UPDATING_DATA: 'Error updating data',
            ERROR_REMOVING_DATA: 'Error removing data',
        },
    },
    FILE_SYSTEM: {
        DATA_DIR_PATH: './.basicrest/data',
    }
}

module.exports = CONSTANTS;