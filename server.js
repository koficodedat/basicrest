const { handlerError } = require('./util/handlers');

const UNCAUGHT_EXCEPTION = 'uncaughtException';
const UNHANDLED_REJECTION= 'unhandledRejection:';

// build fastify
function buildFastify() {
  const fastify = require('fastify')({
    ignoreTrailingSlash: true,
  });
  
  // add global error handler
  fastify.setErrorHandler(handlerError);
  
  // register routes
  fastify.register( require('./routes/base') );
  fastify.register( require('./routes/fetch') );
  fastify.register( require('./routes/save') );
  fastify.register( require('./routes/count') );
  fastify.register( require('./routes/update') );
  fastify.register( require('./routes/remove') );

  return fastify;
}

// start server
const startServer = async (port) => {
  const fastify = buildFastify();
  try {
    const info = `Server running on ${port}`;

    await fastify.listen(port);
  
    console.info(info);
    fastify.log.info(info);

    return fastify;
  } catch(err) {
    console.error(err);
    fastify.log.error(err);
    
    process.exit(1);
  }
}

process.on(UNCAUGHT_EXCEPTION, err => console.error(`${UNCAUGHT_EXCEPTION}: `, err));
process.on(UNHANDLED_REJECTION, err => console.error(`${UNHANDLED_REJECTION}: `, err));

module.exports = {
  buildFastify,
  startServer,
};