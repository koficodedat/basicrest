const assert = require('assert');
const proxyquire = require('proxyquire');

const { HTTP_CODES } = require('../../constants');

let generateOperation;
describe('[util/generate-operation.js]', () => {
  before(() => {
    generateOperation = proxyquire('../../util/generate-operation', {})
  });

  it('should throw error of empty data when generateOperation(..) is called with invalid operator', async () => {
    assert.throws(
      () => generateOperation('field', 'op', 'value'),
      {
        message: 'op is an invalid operator',
        code: HTTP_CODES.BAD_REQUEST,
        cause: null,
        optionals: {}
      }
    )
  });

  it('should throw error of wrong type value when generateOperation(..) is called with $and operator and non array value', async () => {
    assert.throws(
      () => generateOperation('field', '$and', 'value'),
      {
        message: 'value must be an array of values for $and operator',
        code: HTTP_CODES.BAD_REQUEST,
        cause: null,
        optionals: {}
      }
    )
  });

  it('should throw error of wrong type value when generateOperation(..) is called with $in operator and non array value', async () => {
    assert.throws(
      () => generateOperation('field', '$in', 'value'),
      {
        message: 'value must be an array of values for $in operator',
        code: HTTP_CODES.BAD_REQUEST,
        cause: null,
        optionals: {}
      }
    )
  });

  it('should throw error of not yet implemented when generateOperation(..) is called with $where operator', async () => {
    assert.throws(
      () => generateOperation('field', '$where', 'value'),
      {
        message: '$where is not yet implemented',
        code: HTTP_CODES.NOT_IMPLEMENTED,
        cause: null,
        optionals: {}
      }
    )
  });

  it('should return equality operation when generateOperation(..) is called with $eq operator', () => {
    assert.deepStrictEqual(generateOperation('field', '$eq', 'value'), { field: 'value' })
  });

  it('should return exists operation when generateOperation(..) is called with $exists operator', () => {
    assert.deepStrictEqual(generateOperation('field', '$exists', 'value'), { field: { $exists: 'value' } })
  });

  it('should return or operation when generateOperation(..) is called with $or operator', () => {
    assert.deepStrictEqual(
        generateOperation('field', '$or', ['value1', 'value2', 'value3']), 
        { $or: [ { field: 'value1' }, { field: 'value2' }, { field: 'value3' } ]  }
    )
  });

  it('should return in operation when generateOperation(..) is called with $in operator', () => {
    assert.deepStrictEqual(
        generateOperation('field', '$in', ['value1', 'value2', 'value3']), 
        { field: { $in: ['value1', 'value2', 'value3'] }  }
    )
  });
});
