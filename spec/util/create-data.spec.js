const assert = require('assert');
const proxyquire = require('proxyquire');
const { stub } = require('sinon');

const { HTTP_CODES, LOCALIZATION } = require('../../constants');

let createData, stubs;
describe('[util/create-data.js]', () => {
  beforeEach(() => {
    stubs = {
      dbInsertStub: stub().returns(true),
      saveFieldsStub: stub().returns(true),
      getFieldsToSaveStub: stub().returns({ fieldOne: 'valueOne', fieldTwo: 'valueTwo' }),
    }
    createData = proxyquire('../../util/create-data', {
      '../db': () => ({ insert: stubs.dbInsertStub }),
      './save-data-set-fields': stubs.saveFieldsStub,
      './get-fields-to-save': stubs.getFieldsToSaveStub,
    })
  });

  it('should throw error of empty data when createData(..) is called with empty array data', async () => {
      await assert.rejects(
        async () => {
          await createData('label', [], 1000);
        },
        {
          message: LOCALIZATION.EN.ERROR_CREATING_DATA,
          code: HTTP_CODES.INTERNAL_SERVER_ERROR,
          cause: LOCALIZATION.EN.CANNOT_SAVE_EMPTY_DATA,
          optionals: {}
        }
      );
  });

  it('should throw error of empty data when createData(..) is called with empty object data', async () => {
    await assert.rejects(
      async () => {
        await createData('label', [], 1000);
      },
      {
        message: LOCALIZATION.EN.ERROR_CREATING_DATA,
        code: HTTP_CODES.INTERNAL_SERVER_ERROR,
        cause: LOCALIZATION.EN.CANNOT_SAVE_EMPTY_DATA,
        optionals: {}
      }
    );
  });

  it('should pass with true and assert calls when an object is passed in', async () => {
    const response = await createData('label', { 'firstname': 'kofi' }, 1000);

    assert(response, true);
    assert(stubs.dbInsertStub.calledOnce, true);
    assert(stubs.saveFieldsStub.calledOnce, true);
    assert(stubs.getFieldsToSaveStub.calledOnce, true);
  });

  it('should pass with true and assert calls when an array of ojects are passed in', async () => {
    const response = await createData('label', [{ 'firstname': 'kofi' }], 1000);

    assert(response, true);
    assert(stubs.dbInsertStub.calledOnce, true);
    assert(stubs.saveFieldsStub.calledOnce, true);
    assert(stubs.getFieldsToSaveStub.calledOnce, true);
  });
});
