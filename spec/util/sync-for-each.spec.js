const assert = require('assert');

const asyncForEach = require('../../util/sync-for-each');

describe('[util/async-for-each.js]', () => {
    it('should have valid sum at interval points', async () => {
        const numbers = [0,1,2,3,4,5,6,7,8,9];
        const intervalSum = [0,1,3,6,10,15,21,28,36,45];
        const intervalTime = [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000];

        const delayPromise = duration => callback => (number, index, sum) => {
            return new Promise(
                (resolve, reject) => {
                    setTimeout(
                        () => resolve( callback(number, index, sum, duration) ), duration
                    )
                }
            )
        }

        const sumCheck = (number, index, sum = 0, duration) => {
            sum += number;
            assert.strictEqual(sum, intervalSum[index]);
            assert.strictEqual(index === 0 ? duration : (index + 1) * duration, intervalTime[index]);
            return true;
        }

        await asyncForEach(
            numbers, async (number, index) => {
                delayPromise(1000)(sumCheck)(number, index, index === 0 ? 0 : intervalSum[index - 1])
            }
        )
    });
});
