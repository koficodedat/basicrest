const assert = require('assert');
const proxyquire = require('proxyquire');
const { stub } = require('sinon');

const { HTTP_CODES, LOCALIZATION } = require('../../constants');

let getCount, stubs;
describe('[util/get-count.js]', () => {
  before(() => {
    stubs = {
      dbCountStub: stub().returns(5),
    }

    getCount = proxyquire('../../util/get-count', {
      '../db': () => ({ count: stubs.dbCountStub })
    })
  });

  it('should throw error when filters field is of wrong type', async () => {
    await assert.rejects(
      async () => {
        await getCount('label', 'filters', 1000);
      },
      {
        message: LOCALIZATION.EN.ERROR_GETTING_COUNT,
        code: HTTP_CODES.INTERNAL_SERVER_ERROR,
        cause: 'filters.reduce is not a function',
        optionals: {}
      }
    );
    assert(stubs.dbCountStub.notCalled, true);
  });

  it('should pass with 5 as return value and assert calls', async () => {
    assert.strictEqual(await getCount('label', [{ field: 'field', op: '$in', value: ['value'] }], 1000), 5);
    assert(stubs.dbCountStub.called, true);
  });
});
