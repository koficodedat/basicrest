
const assert = require('assert');
const proxyquire = require('proxyquire');
const { stub } = require('sinon');

let saveDataSetFields, stubs;
describe('[util/save-data-set-fields.js]', () => {
  beforeEach(() => {
    stubs = {
      findOneStub: stub().callsFake(
        data => {
            const { _$ref } = data;
            if( _$ref === 'label' ) return { fields: 'field1, field2' };
            return '';
        }
      ),
      insertStub: stub().returns(true),
      updateStub: stub().returns(true),
    }

    saveDataSetFields = proxyquire('../../util/save-data-set-fields', {
      '../db': () => ({
          findOne:  stubs.findOneStub,
          insert: stubs.insertStub,
          update: stubs.updateStub
        }),
    })
  });

  it('should pass with proper assertions if no fields was found in doc', async () => {
    await saveDataSetFields('none', 'field1');
    assert(stubs.findOneStub.called, true);
    assert(stubs.insertStub.called, true);
    assert(stubs.updateStub.notCalled, true);
  });

  it('should pass with proper assertions if no fields was found in doc', async () => {
    await saveDataSetFields('label', 'field3');
    assert(stubs.findOneStub.called, true);
    assert(stubs.insertStub.notCalled, true);
    assert(stubs.updateStub.called, true);
  });
});
