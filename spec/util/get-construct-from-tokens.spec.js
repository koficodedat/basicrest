const assert = require('assert');
const proxyquire = require('proxyquire');

let getConstructFromTokens, stubs;
describe('[util/get-construct-from-tokens.js]', () => {
  before(() => {
    getConstructFromTokens = proxyquire('../../util/get-construct-from-tokens', {})
  });

  it('should return valid array of tokens based on content passed in', async () => {
      assert.deepStrictEqual(
        getConstructFromTokens([],  ['{', 'firstname', 'lastname', 'bio', '{', 'age', 'height', '}', '}']), 
        [ '{', 'firstname', 'lastname', [ 'bio.age', 'bio.height' ], '}' ]
    )
  });
});
