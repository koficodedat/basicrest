const assert = require('assert');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const stub = require('sinon').stub;

let createIndexes, stubs;
describe('[util/create-indexes.js]', () => {
  stubs = {
    ensureIndexStub: stub().resolves(true),
  }
  before(() => {
    createIndexes = proxyquire('../../util/create-indexes', {
      '../db': () => ({ ensureIndex: stubs.ensureIndexStub }),
    })
  });

  it('should pass and assert calls', async () => {
    await createIndexes(['firstname', 'lastname', 'bio'], 1000);

    assert(stubs.ensureIndexStub.calledThrice, true);
    sinon.assert.calledWith(stubs.ensureIndexStub.firstCall, { fieldName: 'firstname' });
    sinon.assert.calledWith(stubs.ensureIndexStub.secondCall, { fieldName: 'lastname' });
    sinon.assert.calledWith(stubs.ensureIndexStub.thirdCall, { fieldName: 'bio' });
  });
});
