const assert = require('assert');
const proxyquire = require('proxyquire');
const { stub } = require('sinon');

const { HTTP_CODES, LOCALIZATION } = require('../../constants');

let getData, stubs;
describe('[util/get-data.js]', () => {
  before(() => {
    stubs = {
      findOneStub: stub().returns({ fields: 'field1, field2' }),
      findStub: stub().returns({
          skip: stub().returns({
              limit: stub().returns({
                  sort:  stub().returns({
                      field1: 'field1',
                      field2: 'field2',
                      field3: 'field3',
                  })
              })
          })
      }),
    }

    getData = proxyquire('../../util/get-data', {
      '../db': () => ({ 
          findOne:  stubs.findOneStub,
          find: stubs.findStub,
        })
    })
  });

  it('should throw error when filters field is of wrong type', async () => {
    await assert.rejects(
      async () => {
        await getData('label', ['field1', 'field2'], '', {}, 1000);
      },
      {
        message: LOCALIZATION.EN.ERROR_GETTING_DATA,
        code: HTTP_CODES.INTERNAL_SERVER_ERROR,
        cause: 'filters.reduce is not a function',
        optionals: {}
      }
    );
    assert(stubs.findStub.notCalled, true);
  });

  it('should pass with proper return value and assert calls', async () => {
    assert.deepStrictEqual(await getData('label', ['field1', 'field2'], [{ field: 'field', op: '$or', value: ['value'] }], {}), { field1: 'field1', field2: 'field2', field3: 'field3' }, 1000);
    assert(stubs.findOneStub.notCalled, true);
    assert(stubs.findStub.called, true);
  });

  it('should pass with proper return value and assert calls', async () => {
    assert.deepStrictEqual(await getData('label', [], [], { sort: 'fieldOne|fieldTwo:desc' }), { field1: 'field1', field2: 'field2', field3: 'field3' }, 1000);
    assert(stubs.findOneStub.called, true);
    assert(stubs.findStub.called, true);
  });
});
