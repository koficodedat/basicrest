const assert = require('assert');
const proxyquire = require('proxyquire');
const { stub } = require('sinon');

const { HTTP_CODES, LOCALIZATION } = require('../../constants');

let removeData, stubs;
describe('[util/remove-data.js]', () => {
  before(() => {
    stubs = {
      removeStub: stub().returns(true),
    }

    removeData = proxyquire('../../util/remove-data', {
      '../db': () => ({ remove: stubs.removeStub })
    })
  });

  it('should throw error when filters field is of wrong type', async () => {
    await assert.rejects(
      async () => {
        await removeData('label', '');
      },
      {
        message: LOCALIZATION.EN.ERROR_REMOVING_DATA,
        code: HTTP_CODES.INTERNAL_SERVER_ERROR,
        cause: 'filters.reduce is not a function',
        optionals: {}
      }
    );
    assert(stubs.removeStub.notCalled, true);
  });

  it('should pass with proper return value and assert calls', async () => {
    assert.strictEqual(await removeData('label', [{ field: 'field', op: '$in', value: ['value'] }]), true);
    assert(stubs.removeStub.called, true);
  });
});
