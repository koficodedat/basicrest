const assert = require('assert');
const proxyquire = require('proxyquire');

let get
describe('[util/get.js]', () => {
  before(() => {
    get = proxyquire('../../util/get', {})
  });

  it('should return actual value from path', async () => {
      assert.strictEqual( get( { one: [ 'two', 'three' ] }, 'one.0', 'four'), 'two' );
      assert.strictEqual( get( { one: [ 'two', 'three' ] }, 'one.5', 'four'), 'four' );
      assert.strictEqual( get( { one: [ 'two', 'three' ] }, 'one.5'), null );
  });
});
