const assert = require('assert');
const proxyquire = require('proxyquire');
const { stub } = require('sinon');

let createDafaultIndexes, stubs;
describe('[util/create-default-indexes.js]', () => {
  stubs = {
    createIndexesStub: stub().returns(true),
  }
  before(() => {
    createDafaultIndexes = proxyquire('../../util/create-default-indexes', {
      './create-indexes':stubs.createIndexesStub,
    })
  });

  it('should pass with true and assert calls', async () => {
    const response = await createDafaultIndexes();

    assert(response, true);
    assert(stubs.createIndexesStub.calledOnce, true);
  });
});
