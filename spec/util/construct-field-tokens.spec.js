const assert = require('assert');

const getContstructFromTokens = require('../../util/construct-field-tokens');

describe('[util/construct-field-tokens.js]', () => {
    it('should return an empty array if argument is not of string type', () => {
      assert.deepStrictEqual(getContstructFromTokens(8), []);
    });

    it('should return an empty array for an empty string', () => {
      assert.deepStrictEqual(getContstructFromTokens(''), []);
    });

    it('should return an array of two tokens with valid content', () => {
      assert.deepStrictEqual(getContstructFromTokens('{ }'), ['{', '}']);
    });

    it('should return an array of three tokens with valid content', () => {
      assert.deepStrictEqual(getContstructFromTokens('{ firstname }'), ['{', 'firstname', '}']);
    });

    it('should return an array of three tokens with valid content', () => {
      assert.deepStrictEqual(getContstructFromTokens('{ firstname,  }'), ['{', 'firstname', '}']);
    });

    it('should return an array of three tokens with valid content', () => {
      assert.deepStrictEqual(getContstructFromTokens('{ firstname ,  }'), ['{', 'firstname', '}']);
    });

    it('should return an array of five tokens with valid content', () => {
      assert.deepStrictEqual(
        getContstructFromTokens(`
        {
          firstname
          lastname
          bio {
  
          }
        }`), ['{', 'firstname', 'lastname', [], '}']);
    });

    it('should return an array of five tokens with valid content', () => {
      assert.deepStrictEqual(
        getContstructFromTokens(`
        {
          firstname
          lastname
          bio {
            age
            height
          }
        }`), ['{', 'firstname', 'lastname', [ 'bio.age', 'bio.height' ], '}']);
    });

    it('should return an array of seven tokens with valid content', () => {
      assert.deepStrictEqual(
        getContstructFromTokens(`
        {
          firstname
          lastname
          bio {
            age
            height
            diet {
              drink
              carbs
              proteins
              fruits
              vegetables
            }
          }
          residence {
            street
            zipcode
          }
          work {
            type
            isfulltime
            salary
            roles
          }
        }`), 
        [
          '{', 
          'firstname', 
          'lastname', 
          [ 
            'bio.age', 
            'bio.height',
            'bio.diet.drink',
            'bio.diet.carbs',
            'bio.diet.proteins',
            'bio.diet.fruits',
            'bio.diet.vegetables'
          ], 
          [ 
            'residence.street', 
            'residence.zipcode' 
          ], 
          [
            'work.type',
            'work.isfulltime',
            'work.salary',
            'work.roles'
          ],
          '}'
        ]
      );
    });

    it('should return an array of four tokens with (BEST GUESS ASSUMPTION) content - [FAIL GRACEFULLY]', () => {
      assert.deepStrictEqual(
        getContstructFromTokens(`
        {
          firstname
          lastname
          bio {
            age
            height
          residence`), ['{', 'firstname', 'lastname', [ 'bio.age', 'bio.height', 'bio.residence' ]]);
    });

    it('should return an array of eight tokens with (BEST GUESS ASSUMPTION) content - [FAIL GRACEFULLY]', () => {
      assert.deepStrictEqual(
        getContstructFromTokens(`
        {
          firstname
          lastname
          bio 
            age
            height
          }
          residence`), ['{', 'firstname', 'lastname', 'bio', 'age', 'height', '}', 'residence']);
    });


    // it('should return an array of two tokens with valid content', () => {
    //   console.log('tokens: ', getContstructFromTokens(`
    //   {
    //     firstname
    //     lastname
    //     bio 
    //       age
    //       height
    //     }
    //     residence
    //   `))
    // });
});
