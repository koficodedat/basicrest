const assert = require('assert');

const { isOpeningBracket, isClosingBracket } = require('../../util/bracket-check');

describe('[util/bracket-check.js]', () => {
    it('should return true for opening bracket', () => {
      assert.equal(isOpeningBracket('{'), true);
    });

    it('should return false for opening bracket', () => {
      assert.equal(isOpeningBracket('}'), false);
    });

    it('should return true for closing bracket', () => {
      assert.equal(isClosingBracket('}'), true);
    });

    it('should return false for closing bracket', () => {
      assert.equal(isClosingBracket('{'), false);
    });
});
