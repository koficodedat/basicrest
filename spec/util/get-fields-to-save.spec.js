const assert = require('assert');

const getFieldsToSave = require('../../util/get-fields-to-save');

describe('[util/get-fields-to-save.js]', () => {
    it('should return expected concatenated strings from object', () => {
      const object = {
        fieldOne: 'valueOne',
        fieldTwo: 'valueTwo'
      }
      assert.equal(getFieldsToSave(object), 'fieldOne,fieldTwo');
    });

    it('should return expected concatenated strings from array of objects', () => {
      const array = [
        {
          fieldOne: 'valueOne',
          fieldTwo: 'valueTwo'
        },
        {
          fieldOne: 'valueOne',
          fieldThree: 'valueThree'
        },
        {
          fieldTwo: 'valueOne',
          fieldThree: 'valueThree',
          fieldFour: 'valueFour',
          fieldFive: 'valueFive',
        }
      ];

      assert.equal(getFieldsToSave(array), 'fieldOne,fieldTwo,fieldThree,fieldFour,fieldFive');
    });
});
