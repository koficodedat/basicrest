
const assert = require('assert');
const proxyquire = require('proxyquire');
const { stub } = require('sinon');

const { HTTP_CODES, LOCALIZATION } = require('../../constants');

let updateData, stubs;
describe('[util/update-data.js]', () => {
  before(() => {
    stubs = {
      updateStub: stub().returns(true),
      saveFieldsStub: stub().returns(true),
      getFieldsToSaveStub: stub().returns({ fieldOne: 'valueOne', fieldTwo: 'valueTwo' }),
    }

    updateData = proxyquire('../../util/update-data', {
      '../db': () => ({ update: stubs.updateStub }),
      './save-data-set-fields': stubs.saveFieldsStub,
      './get-fields-to-save': stubs.getFieldsToSaveStub,
    })
  });

  it('should throw error when filters field is of wrong type', async () => {
    await assert.rejects(
      async () => {
        await updateData('label', '', {}, {}, 1000);
      },
      {
        message: LOCALIZATION.EN.ERROR_UPDATING_DATA,
        code: HTTP_CODES.INTERNAL_SERVER_ERROR,
        cause: 'filters.reduce is not a function',
        optionals: {}
      }
    );
    assert(stubs.updateStub.notCalled, true);
    assert(stubs.saveFieldsStub.notCalled, true);
    assert(stubs.getFieldsToSaveStub.notCalled, true);
  });

  it('should pass with proper return value and assert calls', async () => {
    assert.strictEqual(await updateData('label', [{ field: 'field', op: '$in', value: ['value'] }],  {}, {}, 1000), true);
    assert(stubs.updateStub.calledOnce, true);
    assert(stubs.saveFieldsStub.calledOnce, true);
    assert(stubs.getFieldsToSaveStub.calledOnce, true);
  });
});
