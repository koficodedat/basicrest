
const assert = require('assert');
const noop = require('lodash.noop');
const proxyquire = require('proxyquire');
const { stub } = require('sinon');

let server, stubs;
describe('[server.js]', () => {
  before(() => {
    stubs = {
        setErrorHandlerStub: stub().callsFake(noop),
        registerStub: stub().callsFake(noop),
        listenStub: stub().callsFake(noop),
        infoStub: stub().callsFake(noop),
        errorStub: stub().callsFake(noop),
    }

    server = proxyquire('../server', {
      'fastify': stub().returns({
        setErrorHandler: stubs.setErrorHandlerStub,
        register: stubs.registerStub,
        listen: stubs.listenStub,
        log: {
            info: stubs.infoStub,
            error: stubs.errorStub,
        }
      })
    })
  });

  it('should verify server is behaving accordingly', async () => {
      await server.startServer(0);
      assert(stubs.setErrorHandlerStub.called, true);
      assert(stubs.registerStub.called, true);
      assert(stubs.listenStub.called, true);
      assert(stubs.infoStub.called, true);
      assert(stubs.errorStub.notCalled, true);
  });
});
