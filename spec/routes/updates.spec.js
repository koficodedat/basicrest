const assert = require('assert');

const { buildFastify } = require('../../server');
const { HTTP_CODES } = require('../../constants');

const INTERNAL_SERVER_ERROR = 'Internal Server Error';

let fastify;
describe('[routes/update.js]', () => {
    before(() => {
        fastify = buildFastify();
        fastify.server = {
            address: () => {
                return {
                    port: 1000,
                }
            }
        }
    });

    after(() => {
        fastify.close();
    });

    describe('[negative testing]', () => {
        it('PUT /update/label route should fail if request body is non existent', () => {
            fastify.inject({
                method: 'PUT',
                url: '/update/label',
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                    JSON.parse(response.payload), 
                    { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: 'body should be object' }
                  );
              })
        });
    
        it('PUT /update/label route should fail if request body is an empty object', () => {
            fastify.inject({
                method: 'PUT',
                url: '/update/label',
                payload: {},
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                      JSON.parse(response.payload), 
                      { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: `body should have required property 'filters'` }
                    );
              })
        });
    
        it('PUT /update/label route should fail if request body does not contain update object', () => {
            fastify.inject({
                method: 'PUT',
                url: '/update/label',
                payload: {
                    filters: []
                },
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                      JSON.parse(response.payload), 
                      { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: `body should have required property 'update'` }
                    );
              })
        });
    
        it('PUT /update/label route should fail if request body contains wrong type of child objects', () => {
            fastify.inject({
                method: 'PUT',
                url: '/update/label',
                payload: {
                    filters: {}
                },
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                      JSON.parse(response.payload), 
                      { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: 'body.filters should be array' }
                    );
              })
        });
    });

    describe('[for code coverage]', () => {
        it('_', () => {
            fastify.inject({
                method: 'PUT',
                url: '/update/label',
                payload: {
                    update: {},
                    filters: []
                },
            });
        });
    });
});