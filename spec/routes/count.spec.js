const assert = require('assert');

const { buildFastify } = require('../../server');
const { HTTP_CODES } = require('../../constants');

const INTERNAL_SERVER_ERROR = 'Internal Server Error';

let fastify;
describe('[routes/count.js]', () => {
    before(() => {
        fastify = buildFastify();
        fastify.server = {
            address: () => {
                return {
                    port: 1000,
                }
            }
        };
    });

    after(() => {
        fastify.close();
    });

    describe('[negative testing]', () => {
        it('POST /count/label route should fail if request body is not null or an object', () => {
            fastify.inject({
                method: 'POST',
                url: '/count/label',
                payload: [],
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                      JSON.parse(response.payload), 
                      { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: 'body should be object,null' }
                    );
              })
        });
    });

    describe('[for code coverage]', () => {
        it('_', async () => {
            fastify.inject({
                method: 'POST',
                url: '/count/label',
            })
        });

        it('_', async () => {
            fastify.inject({
                method: 'GET',
                url: '/count/label',
            })
        });
    })

});