const assert = require('assert');

const { buildFastify } = require('../../server');
const { HTTP_CODES } = require('../../constants');

const INTERNAL_SERVER_ERROR = 'Internal Server Error';

let fastify;
describe('[routes/fetch.js]', () => {
    before(() => {
        fastify = buildFastify();
        fastify.server = {
            address: () => {
                return {
                    port: 1000,
                }
            }
        }
    });

    after(() => {
        fastify.close();
    });

    describe('[negative testing]', () => {
        it('POST /fetch/label route should fail if request body is not an object or null', () => {
            fastify.inject({
                method: 'POST',
                url: '/fetch/label',
                payload: 2
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                    JSON.parse(response.payload), 
                    { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: 'body should be object,null' }
                  );
              })
        });
    
        it('POST /fetch/label route should fail if request body has a wrongly formatted fields property', () => {
            fastify.inject({
                method: 'POST',
                url: '/fetch/label',
                payload: {
                    fields: `
                    { 
                        embedded {
                            alpha
                    
                    `
                }
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                      JSON.parse(response.payload), 
                      { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: `body.fields should match pattern \"^([\\\n\\\t\\s]*{)([A-z0-9_,{}\\\n\\\t\\s]+)(}[\\\n\\\t\\s]*)$\"` }
                    );
              })
        });
    });

    describe('[for code coverage]', () => {
        it('_', () => {
            fastify.inject({
                method: 'POST',
                url: '/fetch/label',
                payload: {
                    fields: `
                    { 
                        embedded {
                            alpha
                        }
                    }
                    `
                }
            })
        });

        it('_', () => {
            fastify.inject({
                method: 'GET',
                url: '/fetch/label',
            })
        });
    });
});