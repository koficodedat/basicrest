const assert = require('assert');

const { buildFastify } = require('../../server');
const { HTTP_CODES } = require('../../constants');

const INTERNAL_SERVER_ERROR = 'Internal Server Error';

let fastify;
describe('[routes/remove.js]', () => {
    before(() => {
        fastify = buildFastify();
        fastify.server = {
            address: () => {
                return {
                    port: 1000,
                }
            }
        }
    });

    after(() => {
        fastify.close();
    });

    describe('[negative testing]', () => {
        it('DELETE /remove/label route should fail if request body is non existent', () => {
            fastify.inject({
                method: 'DELETE',
                url: '/remove/label',
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                      JSON.parse(response.payload), 
                      { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: 'body should be object' }
                    );
              })
        });
    
        it('DELETE /remove/label route should fail if request body is an empty object', () => {
            fastify.inject({
                method: 'DELETE',
                url: '/remove/label',
                payload: {}
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                      JSON.parse(response.payload), 
                      { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: `body should have required property 'filters'` }
                    );
              })
        });
    });

    describe('[for code coverage]', () => {
        it('_', () => {
            fastify.inject({
                method: 'DELETE',
                url: '/remove/label',
                payload: {
                    filters: []
                }
            });
        });
    });
});