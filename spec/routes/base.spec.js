const assert = require('assert');

const { buildFastify } = require('../../server');
const { HTTP_CODES, LOCALIZATION } = require('../../constants');

let fastify;
describe('[routes/base.js] - positive testing', () => {
    before(() => {
        fastify = buildFastify();
    });

    after(() => fastify.close())

    it('GET / route should return expected verbiage', () => {
        fastify.inject({
            method: 'GET',
            url: '/'
          }, (err, response) => {
              assert.strictEqual(err, null);
              assert.strictEqual(response.statusCode, HTTP_CODES.OK);
              assert.strictEqual(response.payload, LOCALIZATION.EN.THANKS_FOR_USING_BASIC_REST);
          })
    });
});