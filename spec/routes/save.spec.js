const assert = require('assert');

const { buildFastify } = require('../../server');
const { HTTP_CODES } = require('../../constants');

const INTERNAL_SERVER_ERROR = 'Internal Server Error';

let fastify;
describe('[routes/save.js]', () => {
    before(() => {
        fastify = buildFastify();
        fastify.server = {
            address: () => {
                return {
                    port: 1000,
                }
            }
        }
    });

    after(() => {
        fastify.close();
    });

    describe('[negative testing]', () => {
        it('POST /save/label route should fail if request body is non existent', () => {
            fastify.inject({
                method: 'POST',
                url: '/save/label',
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                    JSON.parse(response.payload), 
                    { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: 'body should be object,array' }
                  );
              })
        });
    
        it('POST /save/label route should fail if request body is of wrong type', () => {
            fastify.inject({
                method: 'POST',
                url: '/save/label',
                payload: 8
              }, (err, response) => {
                  assert.strictEqual(err, null);
                  assert.deepStrictEqual(
                      JSON.parse(response.payload), 
                      { statusCode: HTTP_CODES.INTERNAL_SERVER_ERROR, error: INTERNAL_SERVER_ERROR, message: 'body should be object,array' }
                    );
              })
        });
    });

    describe('[for code coverage]', () => {
        it('_', () => {
            fastify.inject({
                method: 'POST',
                url: '/save/label',
                payload: {}
            });
        });
        
        it('_', () => {
            fastify.inject({
                method: 'POST',
                url: '/save/label',
                payload: {
                    fieldOne: 'fieldOne',
                    fieldTwo: 'fieldTwo'
                }
            });
        });
    });

});