const { startServer } = require('./server');
const createDataFile = require('./util/create-data-file');
const createDefaultIndexes = require('./util/create-default-indexes');
const constructFieldTokens = require('./util/construct-field-tokens');
const getFields = require('./util/get-fields');
const getData = require('./util/get-data');
const createData = require('./util/create-data');
const getCount = require('./util/get-count');
const updateData = require('./util/update-data');
const removeData = require('./util/remove-data');

const { LOCALIZATION } = require('./constants');

const NUMBER = 'number';
const BOOLEAN = 'boolean';

module.exports = class BasicRest {
  constructor(port, override = false) {
    if( !port ) throw new Error(LOCALIZATION.EN.NEEDS_PORT_NUMBER_FOR_SERVER);
    if( typeof port !== NUMBER ) throw new Error(LOCALIZATION.EN.PORT_MUST_BE_A_NUMBER);
    if( typeof override !== BOOLEAN ) throw new Error(LOCALIZATION.EN.OVERRIDE_MUST_BE_A_BOOLEAN);
    
    this.port = port;
    this.override = override;
  }

  async start() {
    const shouldPerformPreliminaryDataWork = createDataFile(this.port, this.override);
    shouldPerformPreliminaryDataWork && ( await createDefaultIndexes(this.port) );

    this.fastify = await startServer(this.port);
  }

  async fetch(label = '', fields = [], filters = [], options = {}){
    try{
      const fieldTokens = constructFieldTokens(fields);
      fields = getFields(fieldTokens);

      const docs = await getData(label, fields, filters, options, this.port);

      return docs;
    }
    catch(err){
      throw err;
    }
  }

  async save(label = '', body = {}){
    try{
      const saved = await createData(label, body, this.port);

      return saved;
    }
    catch(err){
      throw err;
    }
  }

  async count(label = '', filters = []){
    try{
      const count = await getCount(label, filters, this.port);

      return count;
    }
    catch(err){
      throw err;
    }
  }

  async update(label = '', filters = [], update = {}, options = {}){
    try{
      const updated = await updateData(label, filters, update, options, this.port);

      return updated;
    }
    catch(err){
      throw err;
    }
  }

  async remove(label = '', filters = []){
    try{
      const removed = await removeData(label, filters, this.port);
      
      return removed;
    }
    catch(err){
      throw err;
    }
  }

  close(callback) { this.fastify.close(callback); }
}