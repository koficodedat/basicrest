const { HTTP_CODES } = require('../constants')

module.exports = class ResponseError extends Error {
    constructor(message, code = HTTP_CODES.INTERNAL_SERVER_ERROR, cause = null, optionals = {}) {
        super(message);
        this.code = code;
        this.cause = cause;
        this.optionals = optionals;
    }
}