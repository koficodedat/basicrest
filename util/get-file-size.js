const fs = require('fs');

const { FILE_SYSTEM } = require('../constants');

module.exports = (port) => {
    const stats = fs.statSync(`${FILE_SYSTEM.DATA_DIR_PATH}/${port}`);

    const inGb = stats.size / 1000000000.0;
    const inMb = stats.size / 1000000.0;
    const inKb = stats.size / 1000.0;
    const inB = stats.size;

    return is(inGb) ? `${inGb} Gb` :
        (
            is(inMb) ? `${inMb} Mb` :
            (
                is(inKb) ? `${inKb} Kb` : `${inB} b`
            )
        )
}

const is = (value) => value  >= 1;