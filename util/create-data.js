const isEmpty = require('lodash.isempty');

const db = require('../db');
const ResponseError = require('../classes/response-error');
const getFieldsToSave = require('./get-fields-to-save');
const saveDataSetFields = require('./save-data-set-fields');

const { HTTP_CODES, LOCALIZATION } = require('../constants');

module.exports = async (label, dataSet, port) => {
    try {
        if( isEmpty(dataSet) ) 
            throw new ResponseError(LOCALIZATION.EN.CANNOT_SAVE_EMPTY_DATA, HTTP_CODES.BAD_REQUEST);

        const response = await db(port).insert(
            (dataSet instanceof Array) && dataSet.map( data => ({ ...data, '_$ref': label, '_$type': 'dataset'}) ) // bulk save
            ||
            { ...dataSet, '_$ref': label, '_$type': 'dataset'} // single save
        );
        await saveDataSetFields(label, getFieldsToSave(dataSet), port);

        return response ? true : false;
    } catch(err) {
        throw new ResponseError(LOCALIZATION.EN.ERROR_CREATING_DATA, HTTP_CODES.INTERNAL_SERVER_ERROR, err.message);
    }
}