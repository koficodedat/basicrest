const createIndexes = require('./create-indexes');

module.exports = async (port) => await createIndexes(['$ref', '$type'], port);