const ResponseError = require('../classes/response-error');

const { HTTP_CODES } = require('../constants');

module.exports = (field, op, value) => {
    if( !['$lt', '$lte', '$gt', '$gte', '$in', '$nin', '$eq', '$neq', '$exists', '$regex', '$not', '$or', '$and', '$where'].includes(op) ) 
        throw new ResponseError(`${op} is an invalid operator`, HTTP_CODES.BAD_REQUEST);
    
    switch(op) {
        case '$eq':
            return { [field]: value }
        case '$lt':
        case '$lte':
        case '$gt':
        case '$gte':
        case '$neq':
        case '$exists':
        case '$regex':
        case '$not':
            return { [field]: { [ op === '$neq' ? '$eq' : op ]: value } }
        case '$in':
        case '$nin':
            if( !Array.isArray(value) ) 
                throw new ResponseError(`${value} must be an array of values for ${op} operator`, HTTP_CODES.BAD_REQUEST);
            return { [field]: { [op]: value } }
        case '$or':
        case '$and':
            if( !Array.isArray(value) ) 
                throw new ResponseError(`${value} must be an array of values for ${op} operator`, HTTP_CODES.BAD_REQUEST);
            return { [op]: value.map( currentVal => ({ [field]: currentVal }) ) }
        case '$where':
            throw new ResponseError(`${op} is not yet implemented`, HTTP_CODES.NOT_IMPLEMENTED);
    }
}