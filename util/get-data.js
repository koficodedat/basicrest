const isEmpty = require('lodash.isempty');
const get = require('lodash.get');
const merge = require('lodash.merge');

const db = require('../db');
const ResponseError = require('../classes/response-error');
const generateOpertion = require('./generate-operation');

const { HTTP_CODES, LOCALIZATION } = require('../constants');

const NO_SKIP = 0;
const TOP_LIMIT = 1000000;
const ASC = 1;
const DESC = -1;

module.exports = async (label, projections, filters, options, port) => {
    try {
        const { skip, limit, sort } = options;

        if( isEmpty(projections) ) {
            const doc = await db(port).findOne(
                { 
                    _$ref: label,
                    _$type: 'dataset-fields'
                }
            )

            projections = getProjectionsObject( get(doc, 'fields', '') );
        }
        else projections = getProjectionsObject(projections.join());

        const docs = await db(port).find(
            { 
                _$ref: label,
                _$type: 'dataset',
                ...filters.reduce(
                    (accum, filter) => {
                        const { field, op, value } = filter;
                        merge(accum, generateOpertion(field, op, value))
                        return accum;
                    },
                    {}
                )
            },
            projections,
        )
        .skip(skip || NO_SKIP)
        .limit(limit || TOP_LIMIT)
        .sort(getSortObject(sort))
    
        return docs;
    } catch(err) {
        throw new ResponseError(LOCALIZATION.EN.ERROR_GETTING_DATA, HTTP_CODES.INTERNAL_SERVER_ERROR, err.message);
    }
}

const getSortObject = sort => {
    let accumulator = {}

    if(!sort || ( sort && typeof sort !== 'string' )) return accumulator;
    const pipeSplit = sort.split('|').filter( token => token !== '' );

    return pipeSplit.reduce(
        (accum, current) => {
            const colonSplit = current.split(':');
            const [field, order] = colonSplit;

            accum = {
                ...accum,
                [field]: (order !== undefined && order === 'desc') && DESC || ASC // defaults to ascending
            }

            return accum;
        },
        accumulator
    );
}

const getProjectionsObject = sort => {
    let accumulator = {}

    const commaSplit = sort.split(',')

    return commaSplit.reduce(
        (accum, field) => {
            accum = {
                ...accum,
                [field]: 1
            }

            return accum;
        },
        accumulator
    );
}