module.exports = async (array, asyncCallback) => {
    for(let index = 0; index < array.length; index++) {
        await asyncCallback(array[index], index, array);
    }
}