const fs = require('fs');

const { FILE_SYSTEM } = require('../constants');

module.exports = (port, override) => {
  createDataDirPath();
  const shouldPerformPreliminaryDataWork = createDataFilePath(port, override);
  return shouldPerformPreliminaryDataWork;
}

function createDataDirPath() {
  if (!fs.existsSync(FILE_SYSTEM.DATA_DIR_PATH)){
      fs.mkdirSync(FILE_SYSTEM.DATA_DIR_PATH, { recursive: true });
  }
}

function createDataFilePath(port, override) {
  let fileExists = false, shouldPerformPreliminaryDataWork = true;

  if (fs.existsSync( `${FILE_SYSTEM.DATA_DIR_PATH}/${port}` )) fileExists = true;
  if (fileExists && !override) shouldPerformPreliminaryDataWork = false;

  fs.closeSync( fs.openSync(`${FILE_SYSTEM.DATA_DIR_PATH}/${port}`, override ? 'w' : 'a') );

  return shouldPerformPreliminaryDataWork;
}