const merge = require('lodash.merge');

const db = require('../db');
const ResponseError = require('../classes/response-error');
const generateOpertion = require('./generate-operation');

const { HTTP_CODES, LOCALIZATION } = require('../constants');

module.exports = async (label, filters, port) => {
  try {
      const count = await db(port).count(
          { 
              '_$ref': label,
              '_$type': 'dataset',
              ...filters.reduce(
                  (accum, filter) => {
                      const { field, op, value } = filter;
                      merge(accum, generateOpertion(field, op, value));
                      return accum;
                  },
                  {}
              )
          }
      );
  
      return count;
  } catch(err) {
      throw new ResponseError(LOCALIZATION.EN.ERROR_GETTING_COUNT, HTTP_CODES.INTERNAL_SERVER_ERROR, err.message);
  }
}