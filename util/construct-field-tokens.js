const getContstructFromTokens = require('./get-construct-from-tokens');

/**
 * CONSTRUCT FIELD TOKENS
 * @returns an array of separated string tokens based on the content provided.
 */
module.exports = data => {
    if( typeof data !== 'string') return [];

    const trimmed = data.trim();
    const commasToSpace =  trimmed.replace(/,/g, ' ');
    const tabSplit = commasToSpace.split('\t');

    const spaceSplit = tabSplit.reduce(
        (accum, current) => {
            const tokenOnly = current.replace(/\r/g,'').replace(/\n/g,'');
            const noSpaces = tokenOnly.split(' ');

            accum = [
                ...accum,
                ...noSpaces,
            ];

            return accum;
        },
        []
    );

    const bracketSplit = spaceSplit.reduce(
        (accum, current) => {
            const separateClosingBrackets =  current.replace(/}/g, ' }').split(' ');

            accum = [
                ...accum,
                ...separateClosingBrackets,
            ];

            return accum
        },
        []
    );

    const tokens = bracketSplit.filter( token => token.trim() !== '' );
    let parentTokens = [];
    
    const construct = getContstructFromTokens(parentTokens, tokens);
    return construct;
}