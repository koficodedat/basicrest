module.exports = async (array, asyncCallback) => {
    let map = [];

    for(let index = 0; index < array.length; index++) {
        const value = await asyncCallback(array[index], index, array);
        map.push( value )
    }

    return map;
}