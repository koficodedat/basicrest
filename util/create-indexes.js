const db = require('../db');
const forEachSync = require('./sync-for-each');

module.exports = async (fields, port) => {
    await forEachSync(
        fields,
        async fieldName => await db(port).ensureIndex({ fieldName })
    );
}