const get = require('lodash.get');

module.exports = (parent, path, defaultValue = null) => {
    const value = get(parent, path, defaultValue);
    return !value && defaultValue ? defaultValue : value;
}