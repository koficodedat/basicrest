module.exports = {
    isOpeningBracket: token => token.trim() === '{',
    isClosingBracket: token => token.trim() === '}',
}