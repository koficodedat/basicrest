const isEmpty = require('lodash.isempty');

const { isOpeningBracket, isClosingBracket } = require('./bracket-check');

/**
 * GET CONSTRUCT FROM TOKENS
 * @returns an array of properly constructed values for easy traversal when fetching data
 */
module.exports = (parentTokens, tokens) => {
    return tokens.reduce(
        (accum, current, index, array) => {

            const PARENT_TOKENS_IS_EMPTY = isEmpty(parentTokens);
            const PARENT_TOKENS_IS_NOT_EMPTY = !isEmpty(parentTokens);
            const NEXT_TOKEN_IS_OPENING_BRACKET = !isOpeningBracket(current) && (array.length > index + 1) && isOpeningBracket(array[index + 1]);
            const CURRENT_TOKEN_IS_NOT_OPENING_OR_CLOSING_BRACKET = !isOpeningBracket(current) && !isClosingBracket(current);
            const CURRENT_TOKEN_IS_CLOSING_BRACKET = isClosingBracket(current);

            if( PARENT_TOKENS_IS_EMPTY ) {

                accum = [
                    ...accum,
                ];

                if( NEXT_TOKEN_IS_OPENING_BRACKET ) {
                    accum.push([]);
                    parentTokens = [ current ];
                }
                else {
                    accum.push(current);
                }
                
            }
            else if( PARENT_TOKENS_IS_NOT_EMPTY ) {

                if( NEXT_TOKEN_IS_OPENING_BRACKET ) {
                    const currentParentToken = parentTokens[ parentTokens.length - 1 ];
                    parentTokens = [
                        ...parentTokens,
                        `${currentParentToken}.${current}`,
                    ];
                }
                else if( CURRENT_TOKEN_IS_NOT_OPENING_OR_CLOSING_BRACKET ) {
                    const currentParentToken = parentTokens[ parentTokens.length - 1 ];
                    accum[ accum.length - 1 ].push( `${currentParentToken}.${current}` );
                }
            }

            if( CURRENT_TOKEN_IS_CLOSING_BRACKET ) {
                parentTokens.pop();
            }

            return accum;
        },
        []
    );
}