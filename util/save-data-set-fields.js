const get = require('lodash.get');
const union = require('lodash.union');

const db = require('../db');

module.exports = async (label, fields, port) => {
    const doc = await db(port).findOne(
        { 
            _$ref: label,
            _$type: 'dataset-fields'
        }
    );

    const _fields =  get(doc, 'fields', '');

    if( _fields === '' ) {
        await db(port).insert( 
            { 
                fields, 
                _$ref: label, 
                _$type: 'dataset-fields' 
            } 
        )
    }
    else {
        const exists = _fields.split(',');
        const incoming = fields.split(',');
        
        await db(port).update(
            {
                _$ref: label, 
                _$type: 'dataset-fields'
            },
            { $set: { fields: union(incoming, exists).join() } }
        )
    }
}