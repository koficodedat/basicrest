const merge = require('lodash.merge');

const db = require('../db');
const ResponseError = require('../classes/response-error');
const generateOpertion = require('./generate-operation');
const getFieldsToSave = require('./get-fields-to-save');
const saveDataSetFields = require('./save-data-set-fields');

const { HTTP_CODES, LOCALIZATION } = require('../constants');

module.exports = async (label, filters, update, options, port) => {
    try {
      const { upsert = false } = options;

      const response = await db(port).update(
        { 
          '_$ref': label,
          '_$type': 'dataset',
          ...filters.reduce(
              (accum, filter) => {
                  const { field, op, value } = filter;
                  merge(accum, generateOpertion(field, op, value))
                  return accum;
              },
              {}
          )
        },
        { $set: update },
        { upsert },
        { multi: true }
      );
      await saveDataSetFields(label, getFieldsToSave(update), port);
      
      return response ? true : false;
    } catch(err) {
      throw new ResponseError(LOCALIZATION.EN.ERROR_UPDATING_DATA, HTTP_CODES.INTERNAL_SERVER_ERROR, err.message);
    }
}