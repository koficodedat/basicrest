const union = require('lodash.union');

module.exports = dataSet => {
    if( dataSet instanceof Array ) return dataSet.reduce( (accum, current) => union( accum,  Object.keys(current) ), [] ).join();
    else return Object.keys(dataSet).join();
}