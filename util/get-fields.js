const { isOpeningBracket, isClosingBracket } = require('./bracket-check');

const getFields = construct => {
    return construct.reduce(
        (accum, current) => {
            if( current instanceof Array ) {
                accum = [
                    ...accum,
                    ...getFields( current ),
                ]
            }
            else if( !isOpeningBracket(current) && !isClosingBracket(current) ) {
                accum = [
                    ...accum,
                    current,
                ]
            }

            return accum;
        },
        []
    )
}

module.exports = getFields;