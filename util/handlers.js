const ResponseError = require('../classes/response-error');

const { HTTP_CODES } = require('../constants');

module.exports = {
    handlerError: (error, request, reply) => {
        if( error instanceof ResponseError ) {
            reply.code(error.code).send(
                {
                    statusCode: error.code,
                    error: error.cause,
                    message: error.message,
                    ...error.optionals
                }
            )
        }
        else reply.code(error.statusCode || HTTP_CODES.INTERNAL_SERVER_ERROR).send(error);
    },
}