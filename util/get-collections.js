const db = require('../db');
const ResponseError = require('../classes/response-error');

const { HTTP_CODES, LOCALIZATION } = require('../constants');

module.exports = async (port) => {
  try {
    const labels = await db(port).find({ '_$type': 'dataset-fields' });
    return labels.reduce( 
        (labels, label) => {
            labels.push(label);
            return labels;
        },
        [] 
    );
  } catch(err) {
      throw new ResponseError(LOCALIZATION.EN.ERROR_GETTING_COUNT, HTTP_CODES.INTERNAL_SERVER_ERROR, err.message);
  }
}